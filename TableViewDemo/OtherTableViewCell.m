//
//  OtherTableViewCell.m
//  TableViewDemo
//
//  Created by James Cash on 14-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "OtherTableViewCell.h"

@implementation OtherTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
