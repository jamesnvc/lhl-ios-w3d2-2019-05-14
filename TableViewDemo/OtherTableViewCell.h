//
//  OtherTableViewCell.h
//  TableViewDemo
//
//  Created by James Cash on 14-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OtherTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
