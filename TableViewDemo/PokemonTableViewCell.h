//
//  PokemonTableViewCell.h
//  TableViewDemo
//
//  Created by James Cash on 14-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PokemonTableViewCell : UITableViewCell

@property (nonatomic,strong) NSString* pokemonName;

- (void)setPokemon:(NSString*)pokemon typeColour:(UIColor*)typeColour;

@end

NS_ASSUME_NONNULL_END
