//
//  PokemonTableViewCell.m
//  TableViewDemo
//
//  Created by James Cash on 14-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "PokemonTableViewCell.h"

@interface PokemonTableViewCell ()

@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UIView *typeColourView;

@end

@implementation PokemonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPokemon:(NSString*)pokemon typeColour:(UIColor*)colour {
    self.pokemonName = pokemon;
    self.nameLabel.text = pokemon;
    self.typeColourView.backgroundColor = colour;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    NSLog(@"Cell being reused!");
    self.nameLabel.text = @"NAME";
    self.typeColourView.backgroundColor = [UIColor clearColor];
}

@end
