//
//  ViewController.m
//  TableViewDemo
//
//  Created by James Cash on 14-05-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "PokemonTableViewCell.h"
#import "OtherTableViewCell.h"
#import "DetailViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray<NSArray<NSString*>*>* pokemon;
@property (nonatomic,strong) NSArray<NSString*>* sectionTitles;
@property (nonatomic,strong) NSDictionary<NSString*,UIColor*>* typeColours;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:@"OtherTableViewCell" bundle:nil] forCellReuseIdentifier:@"otherCell"];
//    [self.tableView registerClass:[OtherTableViewCell class] forCellReuseIdentifier:@"otherCell2"];

    self.pokemon = @[@[@"Charmander", @"Growlith", @"Vulpix"],
                     @[@"Bulbasaur", @"Oddish"],
                     @[@"Blastoise", @"Magicarp", @"Goldeen", @"Lapras"]
                     ];
    self.sectionTitles= @[@"Fire", @"Grass", @"Water"];
    self.typeColours = @{@"Fire": [UIColor redColor],
                         @"Grass": [UIColor greenColor],
                         @"Water": [UIColor blueColor]
                         };
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"pokemonDetail"]) {
        DetailViewController* dvc = [segue destinationViewController];

//        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
//        NSLog(@"Clicked on %@", indexPath);
//        NSString *name_ = self.pokemon[indexPath.section][indexPath.row];

        PokemonTableViewCell *cell = sender;
        NSString *name = cell.pokemonName;

        dvc.detailItem = name;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.pokemon.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pokemon[section].count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.pokemon[indexPath.section].count) {
        OtherTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"otherCell"];
        cell.nameLabel.text = @"END OF SECTION";
        return cell;
    }
    NSLog(@"Getting cell at section %ld, row %ld", indexPath.section, indexPath.row);
    PokemonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pokemonCell"];

//    if ([cell.textLabel.text isEqualToString:@"Title"]) {
//        cell.textLabel.text = [NSString stringWithFormat:@"Hello %ld/%ld", indexPath.section, indexPath.row];
//    }

    NSString* pokemon = self.pokemon[indexPath.section][indexPath.row];
    UIColor *colour = self.typeColours[self.sectionTitles[indexPath.section]];
    [cell setPokemon:pokemon typeColour:colour];

    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor cyanColor];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.sectionTitles[section];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.pokemon[indexPath.section].count) {
        return 45.0;
    }
    if ([self.pokemon[indexPath.section][indexPath.row] isEqualToString:@"Bulbasaur"]) {
        return 75.0;
    }
    switch (indexPath.section) {
        case 0: return 60.0;
        case 1: return 30.0;
        default: return 45.0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"SELECTED %ld / %ld", indexPath.section, indexPath.row);
}

@end
